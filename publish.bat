call yarn build

del "C:\WWW\__Deploy\Citylogue\WWW\wwwroot\*.*" /s /q

rmdir "C:\WWW\__Deploy\Citylogue\WWW\wwwroot\css" /s /q
rmdir "C:\WWW\__Deploy\Citylogue\WWW\wwwroot\cssmap-continents" /s /q
rmdir "C:\WWW\__Deploy\Citylogue\WWW\wwwroot\images" /s /q
rmdir "C:\WWW\__Deploy\Citylogue\WWW\wwwroot\js" /s /q
rmdir "C:\WWW\__Deploy\Citylogue\WWW\wwwroot\lib" /s /q


xcopy /s/e/y dist "C:\WWW\__Deploy\Citylogue\WWW\wwwroot\"
rem del "C:\WWW\__Deploy\Citylogue\WWW\wwwroot\*.svg" /s /q

mkdir "C:\WWW\__Deploy\Citylogue\WWW\logs"
start C:\WWW\__Deploy\Citylogue\WWW\wwwroot\