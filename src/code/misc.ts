enum WeekDays {
    Monday = 1,
    Tuesday = 2,
    Wednesday = 3,
    Thursday = 4,
    Friday = 5,
    Saturday = 6,
    Sunday = 7
}

export class Tools {
    static getTargetElementIndex(formElements: any, targetElement: any) {
        const formLength = formElements.length
        for (let i = 0; i < formLength; i++) {
          if (formElements[i] === targetElement) {
            return i;
          }
        }
        return -1
    }

    static focusNextInputField(event: any) {
        const formElements = [...event.target.form]
        const formLength = formElements.length
        for (let ti = 0; ti < formLength; ti++) {
            const el = formElements[ ti ]
            if (!el.dataset.tabindex) {
                el.dataset.tabindex = el.tabIndex || ti + 1;
            }
        }
        formElements.sort( (n1, n2) => n1.dataset.tabindex - n2.dataset.tabindex )
        const ndx = Tools.getTargetElementIndex(formElements, event.target)
        if (ndx + 1 < formLength) {
            let nextEl = formElements[ndx + 1];
            if (!nextEl) {
                return false
            }
            if (nextEl.type === 'button') {
                if (ndx + 2 < formLength) {
                    nextEl = formElements[ndx + 1 + 1];
                } else {
                    return false
                }
            }
            nextEl.focus()
            return true
        } else {
            return false
        }
    }

    static isEmptyDate(d?: Date): boolean {
        return !( d && new Date(d).getFullYear() > 2000 )
    }

    static isEmptyString(str?: string): boolean {
        return !( str && str.trim() !== '')
    }

    static getTimePassed(created: Date): string {
        const now = new Date()
        const totalMinutes = ( (now.getTime() - created.getTime()) / 1000 ) % 10000 // up to 10 hours

        const min = Math.floor(totalMinutes / 60)
        const sec = Math.floor(totalMinutes % 60)
        return min + ':' + ( (sec < 10) ? '0' : '' ) + sec
    }

    static createHeaderArray(n: number) {
        const result = []
        for (let i = 0; i < n; i++) {
            result.push( { value: 'id', text: 'Name' } );
        }
        return result
    }

    static getWeekDay(n: number): string {
        if (n === 0) {
            return ''
        }
        return WeekDays[n]
    }
}
