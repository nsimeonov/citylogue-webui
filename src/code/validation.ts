import formatDate from 'date-fns/format'
import parseDate from 'date-fns/parse'
import isValidDate from 'date-fns/isValid'

export class ValidationEngine {
  requiredFieldRules: Array<(v: string) => boolean | string>  = [
    (v: string) => !!v || 'Required'
  ]
  minLengthRules: Array<(v: string) => boolean | string> = [
    (v: string) => !!v || 'Required',
    (v: string) => v.length >= 8 || 'Min 8 characters',
  ]
  emailRules: Array<(v: string) => boolean | string> = [
    (v: string) => !!v || 'Email is required',
    (v: string) => /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/.test(v) || 'E-mail must be valid'
  ]
  phoneRules: Array<(v: string) => boolean | string>  = [
    (v: string) => !!v || 'Phone # is required',
    (v: string) => /^([+]?[\s0-9]+)?(\d{3}|[(]?[0-9]+[)])?([-]?[\s]?[0-9])+$/.test(v) || 'Phone # must be valid'
  ]
  creditCardRules: Array<(v: string) => boolean | string> = [
    (v: string) => !!v || 'Required',
    // (v: string) => v.length >= 12 || 'Min 12 characters',
    (v: string) => valid_credit_card(v) || 'Invalid card number',
  ]
  decimalNumberRules: Array<(v: string) => boolean | string> = [
    (v: string) => !!v || 'Required',
    (v: string) => !isNaN(parseFloat(v)) || 'Invalid number',
  ]
  integerNumberRules: Array<(v: string) => boolean | string> = [
    (v: string) => !!v || 'Required',
    (v: string) => !isNaN(parseFloat(v)) || 'Invalid number',
    (v: string) => Number.isInteger(parseFloat(v)) || 'Number must be an integer',
  ]
  positiveIntegerNumberRules: Array<(v: string) => boolean | string>  = this.integerNumberRules.concat( (v: string) => (v && parseInt(v, 10) > 0 && Number.isInteger(parseInt(v, 10))) || 'Invalid number' )
  positiveDecimalNumberRules: Array<(v: string) => boolean | string>  = this.decimalNumberRules.concat( (v: string) => (v && parseFloat(v) > 0) || 'Invalid number' )
  negativeDecimalNumberRules: Array<(v: string) => boolean | string>  = this.decimalNumberRules.concat( (v: string) => (v && parseFloat(v) < 0) || 'Invalid number' )
  dateRules: Array<(v: string) => boolean | string>  = [
    (v: string) => !!v || 'Required',
    // @ts-ignore  - the definition is invalid: v can be Date | String | Number
    (v: string) => isValidDate(v) || 'Invalid date',
  ]
  passwordRules: Array<(v: string) => boolean | string>  = [
    (v: string) => !!v || 'Required.',
    (v: string) => (v && v.length >= 6) || 'Min 6 characters',
    (v: string) => /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@#$!%*?&])\S{6,}$/.test(v) || 'Password must contain at least one upper case and one lower case character, a digit and a symbol @#$!%*?&'
  ]
  timeRules: Array<(v: string) => boolean | string>  = [
    (v: string) => !!v || 'Required.',
    (v: string) => (v && v.length >= 5) || 'Min 5 characters',
    (v: string) => /^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/.test(v) || 'Incorrect 24h time format'
  ]

  constructor( additionalRules: any ) {
    if (additionalRules) {
      additionalRules(this)
    }
  }
}

function valid_credit_card(cardNum: string) {
  // Accept only digits, dashes or spaces
  if (/[^0-9-\s]+/.test(cardNum)) {
    return false;
  }

  // The Luhn Algorithm. It's so pretty.
  let nCheck = 0
  let bEven = false
  const str = cardNum.replace(/\D/g, '')

  for (let n = str.length - 1; n >= 0; n--) {
    const cDigit = str.charAt(n)
    let nDigit = parseInt(cDigit, 10)

    if (bEven) {
      nDigit *= 2;
      if (nDigit > 9) {
        nDigit -= 9
      }
    }

    nCheck += nDigit
    bEven = !bEven
  }

  return (nCheck % 10) === 0;
}

export interface Form {
  validate(): boolean
  reset(): void
}
