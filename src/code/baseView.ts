import { Component, Vue, Prop } from 'vue-property-decorator'
import { State, Getter, Mutation, Action } from 'vuex-class'
import formatDate from 'date-fns/format'

export const longPressDelay: number = 1000

@Component({
})
export class BaseView extends Vue {
  @Mutation showSnackbar: any

  formatDate(date: Date | string, fmt: string = 'MM/DD/YYYY') {
    if (date === undefined || date === '1900-01-01T00:00:00' || date === '0001-01-01T00:00:00') {
      return '';
    }
    if (typeof date === 'string') {
      return formatDate( new Date(date), fmt )
    }
    return formatDate(date, fmt)
  }

}
