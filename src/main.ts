import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import './registerServiceWorker';
import vuetify from './plugins/vuetify';

import 'material-design-icons-iconfont/dist/material-design-icons.css'

// import '@fortawesome/fontawesome-pro/css/regular.min.css'
// import '@fortawesome/fontawesome-pro/css/light.min.css'
// import '@fortawesome/fontawesome-pro/css/brands.min.css'
import '@fortawesome/fontawesome-pro/css/all.min.css'

import '@/assets/cssmap-continents/cssmap-continents/cssmap-continents.css'
import '@/assets/cssmap-continents/cssmap-continents/cssmap-themes.css'
import '@/assets/cssmap-continents/jquery.cssmap.min.js'

import 'flag-icon-css/css/flag-icon.css'

import '@/assets/gotham.css'

import VueLazyLoad from 'vue-lazyload'
import 'vue-image-lightbox/dist/vue-image-lightbox.min.css'

Vue.config.productionTip = false;
// tslint:disable-next-line:no-var-requires
const VuetifyConfirm = require('vuetify-confirm')
// tslint:disable-next-line:no-var-requires
const GlobalEvents = require('vue-global-events')

Vue.use(GlobalEvents)
Vue.use(VuetifyConfirm)
Vue.use(VueLazyLoad)

new Vue({
  router,
  store,
  // @ts-ignore
  vuetify,
  render: (h) => h(App)
}).$mount('#app');
