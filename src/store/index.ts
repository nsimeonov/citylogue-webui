import Vue from 'vue'
import Vuex, {StoreOptions} from 'vuex'
import {RootState} from './state'

import {auth} from './auth'
import {Snackbar} from './snackbar'
import {CitylogueData} from './CitylogueData'
const debug = process.env.NODE_ENV !== 'production'

Vue.use(Vuex)

const store: StoreOptions<RootState> = {
  strict: debug,
  modules: {
    auth,
    Snackbar,
    CitylogueData,
  }
}

export default new Vuex.Store<RootState>(store)
