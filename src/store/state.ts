import { AuthState } from './auth'

interface RootState {
  auth: AuthState
}

export {
  RootState
}
