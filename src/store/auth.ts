import {Module, MutationTree, ActionTree, GetterTree} from 'vuex'
import {AxiosResponse} from 'axios'
import {RootState} from '@/store/state'
import {API, UnauthorizedRequestError} from '@/api'

class AuthAPI extends API {
    async authenticate(data: { userName: string, password: string }) {
      const response = await this.client.post('User/Authenticate', data)
      return response.data
    }
    async checkToken(): Promise<AuthState | null | undefined> {
      try {
        const response = await this.get('User/Validate')
        const hasValidToken = (response as AxiosResponse<any>)
        if (!hasValidToken || !hasValidToken.data) {
          return null
        }
        return this.store.state.auth
      } catch (e) {
        if (e instanceof UnauthorizedRequestError) {
          if (this.store.state.auth.Ticket != null) {
            this.store.commit('showSnackbar', { message: 'Your session has expired', color: 'error', timeout: 10000 })
          } else {
            // nothing to show
            // this.store.commit('showSnackbar', { message: 'You need to log in', color: 'error', timeout: 10000 })
            return null
          }
        }
      }
      return undefined
    }
}

interface AuthState {
  IsAdmin: boolean
  FullName: string | null
  UserName: string | null
  Ticket: string | null
}

const mutations: MutationTree<AuthState> = {
  login(s, loginInfo: { FullName: string, UserName: string, Ticket: string, IsAdmin: boolean }) {
      s.IsAdmin = loginInfo.IsAdmin,
      s.FullName = loginInfo.FullName,
      s.UserName = loginInfo.UserName
      s.Ticket = loginInfo.Ticket
      localStorage.setItem('IsAdmin', s.IsAdmin.toString())
      localStorage.setItem('FullName', s.FullName)
      localStorage.setItem('UserName', s.UserName)
      localStorage.setItem('Ticket', s.Ticket)
  },
  logout(s) {
    s.FullName = null
    s.UserName = null
    s.Ticket = null
    s.IsAdmin = false
    localStorage.removeItem('IsAdmin')
    localStorage.removeItem('Ticket')
    localStorage.removeItem('FullName')
    localStorage.removeItem('UserName')
  }
}

const actions: ActionTree<AuthState, RootState> = {
  async login({ commit, rootState }, data: { userName: string, password: string }) {
    const api = new AuthAPI({ commit, state: rootState })
    const loginInfo = await api.authenticate(data)
      .catch((error: any) => {
        if (error instanceof UnauthorizedRequestError) {
          throw error
        } else {
          commit('showSnackbar', { message: error.toString(), color: 'error', timeout: 10000 })
        }
      })
    if (!loginInfo) {
      commit('logout')
      return false
    }
    commit('login', loginInfo)
    return loginInfo
  },
  checkToken({ commit, rootState }) {
    const api = new AuthAPI({ commit, state: rootState })
    return api.checkToken()
  }
}

const getters: GetterTree<AuthState, RootState> = {
  loggedIn(s) {
    if (s.Ticket === null) {
      return false
    }
    if (typeof s.Ticket === 'undefined') {
      return false
    }
    return true
  },
  isAdmin(s) {
    return s.IsAdmin;
  },
  userName(s) {
    if (s.Ticket === null) {
      return ''
    }
    if (typeof s.Ticket === 'undefined') {
      return ''
    }
    return s.FullName
  }
}

const state: AuthState = {
  FullName: localStorage.getItem('FullName'),
  UserName: localStorage.getItem('UserName'),
  IsAdmin: localStorage.getItem('IsAdmin') === true.toString(),
  Ticket: localStorage.getItem('Ticket')
}

const auth: Module<AuthState, RootState> = {
  mutations,
  actions,
  getters,
  state
}

export {
  auth,
  AuthState
}
