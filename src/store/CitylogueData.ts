import {Module, MutationTree, ActionTree} from 'vuex'
import {RootState} from '@/store/state'

interface CitylogueDataState {
  loading: boolean
}

const mutations: MutationTree<CitylogueDataState> = {
    setLoading(s, info: { ready: boolean }) {
      s.loading = !info.ready
    }
}

const CitylogueData: Module<CitylogueDataState, RootState> = {
  mutations,
  state: {
    loading: false
  }
}

export {
  CitylogueData,
  CitylogueDataState
}
