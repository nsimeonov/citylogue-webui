import Vue from 'vue'
import Router from 'vue-router'
import store from '@/store'

import HomePage from '@/views/Home.vue'
import CityPage from '@/views/City.vue'
import PrivacyPage from '@/views/Privacy.vue'
import TermsPage from '@/views/Terms.vue'
// const CityPage = () => import('@/views/City.vue')
// const PrivacyPage = () => import('@/views/Privacy.vue')

Vue.use(Router)

const router = new Router({
  // const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    // {
    //   path: '/',
    //   name: 'home',
    //   redirect: { name: 'about' },
    //   meta: { title: 'New Orders' }
    // },

    {
      path: '/',
      name: 'home',
      component: HomePage,
      meta: {
        auth: false,
        title: 'Home',
      }
    },
    {
      path: '/city/:slug',
      name: 'city',
      component: CityPage,
      meta: {
        auth: false,
        title: 'City',
      }
    },
    {
      path: '/privacy-policy',
      name: 'privacy',
      component: PrivacyPage,
      meta: {
        auth: false,
        title: 'Privacy Policy',
      }
    },
    {
      path: '/terms-of-service',
      name: 'terms',
      component: TermsPage,
      meta: {
        auth: false,
        title: 'Terms Of Service',
      }
    },
  ],
})

router.beforeEach((to, from, next) => {
  const requiresLogIn = to.matched.some(record => record.meta.auth)
  const requiresAdminLogIn = to.matched.some(record => record.meta.adminAuth)
  const isLoggedIn = store.getters.loggedIn
  const isAdmin  = store.getters.isAdmin
  const isLoginRoute = to.matched.some(record => record.name === 'login')
  if (requiresLogIn && !isLoggedIn || requiresAdminLogIn && !isAdmin) {
      return next({ name: 'login', params: { nextURL: to.fullPath } })
  }
  if (isLoggedIn && isLoginRoute) {
      return next({ name: 'home' })
  }
  next()
})

export default router
