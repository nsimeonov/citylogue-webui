import {AxiosResponse} from 'axios'
import {API} from '@/api'
import {Continent} from './continent'
import {Country} from './country'

export interface City {
  ID: number
  Name: string
  Slug: string
  Description: string
  CountryID: number
  VisitTypeID: number
  Rating: number
  RatingsCount: number
  CountryRating: number
  CountryRatingsCount: number
  NumCitiesContinent: number
  NumCitiesWorldwide: number
  GlobalPosotionContinent: number
  GlobalPosotionWorldwide: number
  Latitude: number
  Longitude: number
  Airports: string
  Population: string
  RecommendedStay: string
  SafetyRating: number
  CleanlinessRaitng: number
  FoodQualityRating: number
  RequiredBudget: number
  CEOWorldSafetyRating: string
  MercerQualityOfLiving: string
  CoverImage: CityImage
  ImageGallery: CityImage[]
  Country: Country

  UserEmail?: string
  UserVote?: Vote

  Reviews?: Review[]
}

export interface CityImage {
  ID: number
  Title: string
  ImageExtension: string
  Position: number
  DateLastUpdated: string
}

export interface Vote {
  Provider?: string
  NameIdentifier?: string
  Name?: string
  Email?: string
  Rating: number
  ReviewText?: string
}

export interface Review {
  Name: string
  ReviewText: string
  ReviewPositiveVotes: number
  ReviewNegativeVotes: number
  IsMine: boolean
}

export class CityAPI extends API {

  async load(slug: string) {
    const response = await this.get(`Data/city/${slug}`) as AxiosResponse<City>
    return response.data
  }

  async vote(cityID: number, vote: number) {
    const response = await this.get(`city/vote/${cityID}/${vote}`) as AxiosResponse<City>
    return response.data
  }

  async review(cityID: number, text: string) {
    const response = await this.post(`city/review/${cityID}`, { ReviewText: text }) as AxiosResponse<City>
    return response.data
  }

  async reviewVote(reviewID: number, points: number) {
    const response = await this.get(`city/review/${reviewID}/vote/${points}`) as AxiosResponse<City>
    return response.data
  }
}

export function newCity() {
  return {
    ID: 0,
    Name: '',
    Slug: '',
    Description: '',
    CountryID: 0,
    VisitTypeID: 0,
    Rating: 0,
    RatingsCount: 0,
    CountryRating: 0,
    CountryRatingsCount: 0,
    NumCitiesContinent: 0,
    NumCitiesWorldwide: 0,
    GlobalPosotionContinent: 0,
    GlobalPosotionWorldwide: 0,
    Latitude: 0,
    Longitude: 0,
    Airports: '',
    Population: '',
    RecommendedStay: '',
    SafetyRating: 0,
    CleanlinessRaitng: 0,
    FoodQualityRating: 0,
    RequiredBudget: 0,
    CEOWorldSafetyRating: '',
    MercerQualityOfLiving: '',
    CoverImage: {
      ID: 0,
      Title: '',
      ImageExtension: '',
      Position: 0,
      DateLastUpdated: '',
    },
    ImageGallery: [],
    Country: {
      ID: 0,
      Name: '',
      Alpha2: '',
      ContinentID: 0,
      Description: '',
      ImageExtension: '',
      Continent: {
        ID: 0,
        Name: '',
        NameShort: '',
        Code: '',
        HomePageMapID: 0,
        HomePageOrderNum: 9,
      }
    }
  }
}
