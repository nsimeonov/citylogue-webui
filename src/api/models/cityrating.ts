import {AxiosResponse} from 'axios'
import {API} from '@/api'
import {Continent} from './continent'

export interface CityRating {
  CityID?: number
  CityName: string
  CityRating: number
  CitySlug: string
  ContinentID: number
  ImageID: number
  ImageExtension: number
  ImageDateLastUpdated: string
}

export class CityRatingAPI extends API {

  async loadTopWorldWide() {
    const response = await this.get(`Data/top-worldwide`) as AxiosResponse<CityRating[]>
    return response.data
  }

  async loadTopCitiesByContinent() {
    const response = await this.get(`Data/top-by-continents`) as AxiosResponse<{ [id: number]: CityRating[] }>
    return response.data
  }

  async loadContinents() {
    const response = await this.get(`Data/continents`) as AxiosResponse<Continent[]>
    return response.data
  }

  async findByName(q: string) {
    const response = await this.get(`Data/cities-by-name`, {params: {q}}) as AxiosResponse<CityRating[]>
    return response.data
  }

}
