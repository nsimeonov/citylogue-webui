import {AxiosResponse} from 'axios'
import {API} from '@/api'

export interface Continent {
  ID: number
  Name: string
  NameShort: string
  Code: string
  HomePageMapID: number
  HomePageOrderNum: number
}
