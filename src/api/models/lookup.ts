export interface Lookup {
  ID: number
  Name: string
  IsActive: boolean
}
