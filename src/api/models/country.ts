import {Continent} from './continent'

export interface Country {
  ID: number
  Name: string
  Alpha2: string
  ContinentID: number
  Description: string
  ImageExtension: string
  Continent: Continent
}
